<?php
/*
Plugin Name: Generations Church Sermon Notes
Plugin URI: http://dsgnwrks.pro
Description: Adds a "Sermon Notes" content type and a shortcode for including a sermon notes timeline table on a page/post, etc.
Author URI: http://dsgnwrks.pro
Author: Justin Sternberg
Donate link: http://dsgnwrks.pro/give/
Version: 0.1.0
*/

class gcSermonNotes {

	// A single instance of this class.
	public static $instance     = null;
	public static $sermon       = null;

	/**
	 * Creates or returns an instance of this class.
	 * @since  0.1.0
	 * @return gcSermonNotes A single instance of this class.
	 */
	public static function go() {
		if ( self::$instance === null )
			self::$instance = new self();

		return self::$instance;
	}

	public function __construct() {
		require_once( 'lib/GCSermonCPT.php' );
		GCSermonCPT::go();
		add_shortcode( 'sermon-info-table', array( $this, 'sermoninfo_sc' ) );
		add_filter( 'gform_shortcode_form', array( $this, 'show_entries' ), 10, 3 );
		add_action( 'wp_ajax_gc_q_mark_read', array( $this, 'mark_read' ) );
		add_action( 'wp_ajax_nopriv_gc_q_mark_read', array( $this, 'mark_read' ) );
	}

	public function show_entries( $html, $attributes, $content ) {

		if ( ! isset( $attributes['showentries'] ) || $attributes['showentries'] === 'false' )
			return $html;

		if ( ! isset( $attributes['id'] ) )
			return $html;
		if ( class_exists( 'GFFormsModel' ) )
			$entries = GFFormsModel::get_leads( $attributes['id'] );
		elseif ( class_exists( 'RGFormsModel' ) )
			$entries = RGFormsModel::get_leads( $attributes['id'] );
		else
			return $html;

		if ( empty( $entries ) )
			return $html;

		$columns = array();
		if ( is_user_logged_in() ) {
			$columns['markread'] = 'Remove';
			add_action( 'wp_footer', array( $this, 'mark_read_script' ) );
		}

		$columns['date_created'] = 'Submitted';
		$columns[1]              = 'Submission';

		$rows = '';
		$rownum = 2;
		foreach ( $entries as $key => $entry ) {
			if ( isset( $entry['is_read'] ) && $entry['is_read'] )
				continue;
			$entry = RGFormsModel::get_lead( $entry['id'] );
			$class  = $rownum % 2 == 0 ? 'even' : 'odd';
			$rows .= '<tr class="row-'. $rownum .' '. $class .'">'."\n";
			$count = 1;
			foreach ( $columns as $key => $column ) {
				$rows .= '<td class="column-'. $count .'">';
				$rows .= self::get_meta( $key, $entry );
				$rows .= '</td>'."\n";
				$count++;
			}
			$rows .= "</tr>\n";
			$rownum++;
		}

		if ( empty( $rows ) )
			return $html;

		$theads = '';
		$count = 1;
		foreach ( $columns as $key => $column ) {
			$theads .= '
			<th class="column-'. $count .'" role="columnheader" rowspan="1" colspan="1">
				<div>'. $column .'</div>
			</th>
			';
			$count++;
		}

		$table = '
		<table style="width: 100%;" class="tablepress sermoninfo-submitted-questions">
			<thead>
				<tr class="row-1 odd" role="row">
				'. $theads .'
				</tr>
			</thead>
			<tbody  aria-live="polite" aria-relevant="all">
			'. $rows .'
			</tbody>
		</table>
		';
		return $table . $html;
	}

	public function mark_read_script() {
		wp_enqueue_script( 'jquery' );
		?>
		<script type="text/javascript">
			(function($) {
				$('.sermoninfo-submitted-questions').on( 'click', '.mark-read', function(event) {
					event.preventDefault();
					$self = $(this);
					$row  = $self.parents('tr');
					url   = '<?php echo admin_url( '/admin-ajax.php' ); ?>';
					nonce = '<?php echo wp_create_nonce( 'gc_q_mark_read' ); ?>';
					id    = $self.data('id');

					console.log( 'id', id, $row.attr('class'), $self.attr('class') );

					$.ajax({
						type     : "post",
						dataType : "json",
						url      : url,
						data     : {
							action   : 'gc_q_mark_read',
							security : nonce,
							id       : $self.data('id')
						},
						success  : function( response ) {
							if ( response.success == true )
								$row.fadeOut( 400, function() {
									$row.remove();
								});
						}
					});
				});
			})(jQuery);
		</script>
		<?php
	}

	public function mark_read() {
		check_ajax_referer( 'gc_q_mark_read', 'security' );
		if ( isset( $_POST['id'] ) ) {
			RGFormsModel::update_lead_property( absint( $_POST['id'] ), 'is_read', true );
			wp_send_json_success();
		}
		wp_send_json_error();
	}

	public static function sermoninfo_sc( $atts ) {

		$table_options = array(
			'edit'           => is_user_logged_in(),
			'sermon_date'    => false,
			'speaker'        => false,
			'series'         => false,
			'sermon_title'   => true,
			'sermon_notes'   => true,
			'notes_due_date' => true,
			'due_date'       => true,
			'posted_date'    => true,
		);
		$atts = shortcode_atts( array_merge( array(
			'numberposts' => 5,
			'order'       => 'DESC',
			'series_info' => true,
		), $table_options ), $atts, 'sermon-info-table' );

		$do_series = ( isset( $atts['series_info'] ) && $atts['series_info'] );

		$columns = array();
		foreach ( $table_options as $key => $att ) {
			if ( $atts[ $key ] && $atts[ $key ] !== 'false' )
				$columns[] = $key;
			unset( $atts[ $key ] );
		}

		$sermons = self::get_sermons( $atts );

		if ( empty( $sermons ) )
			return '';

		$table = '
		<table class="tablepress sermoninfo">
			<thead>
				<tr class="row-1 odd" role="row">
				';
				foreach ( $columns as $index => $column ) {
					$count = $index + 1;
					$table .= '
					<th class="column-'. $count .'" role="columnheader" rowspan="1" colspan="1">
						<div>'. self::col_label( $column ) .'</div>
					</th>
					';
				}
				$table .= '
				</tr>
			</thead>
			<tbody  aria-live="polite" aria-relevant="all">
			';
			foreach ( $sermons as $key => self::$sermon ) {
				$rownum = $key + 2;
				$class  = $rownum % 2 == 0 ? 'even' : 'odd';
				$table .= '<tr class="row-'. $rownum .' '. $class .'">'."\n";
				foreach ( $columns as $index => $column ) {
					$count = $index + 1;
					$table .= '<td class="column-'. $count .'">';
					if ( $do_series && $column == 'sermon_title' )
						$column = 'series_info';
					$table .= self::get_meta( $column );
					$table .= '</td>'."\n";
				}
				$table .= "</tr>\n";
			}
			if ( $table_options['edit'] ) {
				$table .= '<tr class="row-'. ( $rownum + 1 ) .' '. ( $class == 'odd' ? 'even' : 'odd' ) .'">'."\n";
					$table .= '<td class="column-1" colspan="'. count( $columns ) .'">';
					$table .= '<a href="'. admin_url( '/post-new.php?post_type='. self::$sermon->post_type ) .'" target="_blank" rel="nofollow">+ Add New Sermon</a>';
					$table .= '</td>'."\n";
				$table .= "</tr>\n";
			}
			$table .= '
			</tbody>
		</table>
		';
		return $table;
	}

	public static function get_meta( $column, $entry = array() ) {
		if ( empty( $entry ) ) {
			$s = self::$sermon;
			$m = $s->meta;
			$t = $s->taxonomies;
		}
		switch ( $column ) {
			case 'sermon_title':
				return isset( $s->post_title ) ? $s->post_title : '';
			case 'series_info':
				$title = isset( $s->post_title ) ? $s->post_title : '';
				$title .= isset( $t['series']['value'] ) ? '<br>'. $t['series']['value'] .'' : '';
				$title .= isset( $m['_gc_sermon_date']['value'] ) ? '<br>'. date( 'm\/d\/Y', $m['_gc_sermon_date']['value'] ) : '';
				$title .= isset( $t['speaker']['value'] ) ? '<br>'. $t['speaker']['value'] : '';
				return $title;
			case 'due_date':
				return isset( $m['_gc_due_date']['value'] ) ? date( 'm\/d\/Y', $m['_gc_due_date']['value'] ) : '';
			case 'posted_date':
				return isset( $m['_gc_posted_date']['value'] ) ? date( 'm\/d\/Y', $m['_gc_posted_date']['value'] ) : '';
			case 'sermon_date':
				return isset( $m['_gc_sermon_date']['value'] ) ? date( 'm\/d\/Y', $m['_gc_sermon_date']['value'] ) : '';
			case 'notes_due_date':
				return isset( $m['_gc_notes_due_date']['value'] ) && is_string( $m['_gc_notes_due_date']['value'] ) ? date( 'm\/d\/Y', $m['_gc_notes_due_date']['value'] ) : '';
			case 'sermon_notes':
				return isset( $m['_gc_notes_url']['value'] ) && trim( $m['_gc_notes_url']['value'] )
					? '<a href="'. esc_url( $m['_gc_notes_url']['value'] ) .'" target="_blank" rel="nofollow">view</a>'
					: 'pending';
			case 'speaker':
				return isset( $t['speaker']['value'] ) ? $t['speaker']['value'] : '';
			case 'series':
				return isset( $t['series']['value'] ) ? $t['series']['value'] : '';
			case 'edit':
				return '<a href="'. get_edit_post_link( self::$sermon->ID ) .'" target="_blank" rel="nofollow">edit</a>';
			case 'date_created':
				return isset( $entry[ $column ] ) ? date( 'm\/d\/Y', strtotime( $entry[ $column ] ) ) : '';
			case 1:
			case '1':

				$sermon = isset( $entry[3] ) && $entry[3] !== 'Select Sermon' ? '<h3><strong>'. $entry[3] .'</strong></h3><br>' : '';

				return isset( $entry[1] ) ? do_shortcode( wpautop( $sermon . $entry[1] ) ) : '';
			case 'markread':
				return '<a class="mark-read" href="#" data-id="'. $entry['id'] .'" rel="nofollow">mark read</a>';

		}

	}

	public static function col_label( $col ) {
		$labels = array(
			'sermon_title'   => 'Sermon',
			'due_date'       => 'Questions Due',
			'posted_date'    => 'Questions Posted',
			'sermon_date'    => 'Sermon Date',
			'sermon_notes'   => 'Sermon Notes',
			'notes_due_date' => 'Sermon Notes Due',
			'speaker'        => 'Speaker',
			'series'         => 'Sermon Series',
		);
		return isset( $labels[ $col ] ) ? $labels[ $col ] : ucfirst( $col );
	}

	public static function get_sermons( $args ) {

		$args = wp_parse_args( $args, array(
			'numberposts'		=>	5,
			'orderby'			=>	'meta_value_num',
			'order'				=>	'DESC',
			'post_type'			=>	'sermons',
			'post_status'		=>	'publish',
			'meta_key'        => '_gc_sermon_date',
		) );

		$sermoncpt    = GCSermonCPT::go();
		$taxonomies   = $sermoncpt->taxonomies();
		$cmb          = cmb2_get_metabox( '_gc_dates' );
		$fields       = $cmb->prop( 'fields' );
		$sermons      = array();
		$sermon_posts = get_posts( $args );

		foreach ( $sermon_posts as $key => $sermon ) {

			$sermon->meta = $sermon->taxonomies = array();

			foreach ( $taxonomies as $tax => $label ) {
				$sermon->taxonomies[ $tax ] = array(
					'name'  => $label,
					'value' => get_the_term_list( $sermon->ID, $tax, '', ', ', '' ),
				);
			}
			foreach ( (array) $fields as $field ) {
				$sermon->meta[ $field['id'] ] = array(
					'name'  => $field['name'],
					'value' => get_post_meta( $sermon->ID, $field['id'], 1 ),
				);
			}
			$sermons[] = $sermon;
		}
		return $sermons;
	}
}

function gc_sermon_notes_init() {
	gcSermonNotes::go();
}
add_action( 'plugins_loaded', 'gc_sermon_notes_init', 9999 );
