<?php

require_once( 'cpt-core/CPT_Core.php' );

/**
 * Plugin class that registers the Meetings CPT.
 *
 */
class GCSermonCPT extends CPT_Core {

	public $prefix          = '_gc_';
	public $mb              = array();
	public $taxonomies      = array();
	// A single instance of this class.
	public static $instance = null;

	/**
	 * Creates or returns an instance of this class.
	 * @since  0.1.0
	 * @return GCSermonCPT A single instance of this class.
	 */
	public static function go() {
		if ( self::$instance === null )
			self::$instance = new self();

		return self::$instance;
	}


	function __construct() {

		parent::__construct( array( 'Sermon Plan', 'Sermon Plans', 'sermons' ), array(
			'menu_position' => 5,
			'supports'      => array( 'title' ),
			'taxonomies'    => array_keys( $this->taxonomies() ),
			'public'        => false,
			'rewrite'       => false,
			'has_archive'   => false,
			'show_in_menu'  => function_exists( 'gc_sermons' )
				? 'edit.php?post_type=' . gc_sermons()->sermons->post_type()
				: true,
			'labels' => array(
				'all_items' => 'Sermon Plans',
			),
		) );

		add_action( 'cmb2_init', array( $this, 'fields' ) );
	}

	public function check_for_cmb2() {
		if ( ! defined( 'CMB2_LOADED' ) ) {
			return add_action( 'all_admin_notices', array( $this, 'admin_notice' ) );
		}

		add_action( 'admin_init', array( $this, 'check_for_cmb2' ) );
		add_action( 'add_meta_boxes', array( $this, 'move_boxes' ) );
	}

	public function admin_notice() {
		echo '<div id="message" class="error"><p>Generations Church Sermon Notes plugin requires the CMB2 plugin, and may be missing functionality or broken until CMB2 is installed/activated.</p></div>';
	}

	public function taxonomies() {
		if ( ! empty( $this->taxonomies ) )
			return $this->taxonomies;

		if ( function_exists( 'gc_sermons' ) ) {
			$this->taxonomies = array(
				gc_sermons()->series->taxonomy() => gc_sermons()->series->taxonomy( 'singular' ),
				gc_sermons()->speaker->taxonomy() => gc_sermons()->speaker->taxonomy( 'singular' ),
			);
		} else {
			$this->taxonomies = array(
				'series'  => __( 'Sermon Series' ),
				'speaker' => __( 'Speaker' ),
			);
		}

		return $this->taxonomies;
	}

	public function move_boxes() {
		foreach ( $this->taxonomies() as $tax => $label ) {
			add_meta_box( $tax.'div', $label, 'post_categories_meta_box', $this->post_type(), 'normal', 'default', array( 'taxonomy' => $tax ) );
		}
	}

	public function title( $title ) {
		$screen = get_current_screen();

		if ( isset( $screen->post_type ) && $screen->post_type == $this->post_type() ) {
			$title = 'Sermon Title';
		}

		return $title;
	}

	function columns( $columns ){
		return $columns;
	}

	public function columns_display( $column, $post_id ){
		global $post;
		switch ( $column ) {
			// case 'meeting_sf_id':
			// 	echo get_post_meta( $post->ID, $this->prefix.'sf_id', 1 );
			// break;
		}

		// $meta = $this->meta();

		// foreach ( (array) $meta['meeting_details']['fields'] as $field_id => $field ) {
		// 	if ( $column == $this->prefix.$field_id )
		// 		echo wpautop( get_post_meta( $post->ID, $this->prefix.$field_id, 1 ) );
		// }

	}

	public function fields() {

		$prefix = $this->prefix;

		$cmb = new_cmb2_box( array(
			'id'         => $prefix.'dates',
			'title'      => 'Sermon Questions Info',
			'object_types'  => array( $this->post_type() ),
		) );

		$cmb->add_field( array(
			'name' => 'Sermon Date',
			'id'   => $prefix . 'sermon_date',
			'type' => 'text_date_timestamp',
		) );

		$cmb->add_field( array(
			'name' => 'Questions Due Date',
			'id'   => $prefix . 'due_date',
			'type' => 'text_date_timestamp',
		) );

		$cmb->add_field( array(
			'name' => 'Questions Posted Date',
			'id'   => $prefix . 'posted_date',
			'type' => 'text_date_timestamp',
		) );

		$cmb->add_field( array(
			'name' => 'Sermon Notes Due Date',
			'id'   => $prefix . 'notes_due_date',
			'type' => 'text_date_timestamp',
		) );

		$cmb->add_field( array(
			'name' => 'Sermon Notes URL',
			'desc' => 'Google Docs URL, etc',
			'id'   => $prefix . 'notes_url',
			'type' => 'text_url',
		) );
	}

}
